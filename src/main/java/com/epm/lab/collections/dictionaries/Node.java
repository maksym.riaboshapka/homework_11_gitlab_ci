package com.epm.lab.collections.dictionaries;

public class Node {
    public int keyData;
    public double valueData;
    public Node leftNode;
    public Node rightNode;

    public void displayNode() {
        System.out.print('{');
        System.out.print(keyData);
        System.out.print(", ");
        System.out.print(valueData);
        System.out.print("} ");
    }
}
