package com.epm.lab.collections.dictionaries;

import com.epm.lab.collections.Map;

import java.util.Iterator;

public class MyMap<Key, Value> implements Map {

    private Object[][] pairs;
    private int index;

    public MyMap(int length) {
        pairs = new Object[length][2];
    }


    @Override
    public Object get(Object key) {
        for (int i = 0; i < index; i++) {
            if (key.equals(pairs[i][0])) {
                return pairs[i][1];
            }
        }
        return null;
    }

    @Override
    public void put(Object key, Object value) {
        if (index >= pairs.length) {
            throw new ArrayIndexOutOfBoundsException();
        }
        pairs[index++] = new Object[]{key, value};
    }

    public String toString() {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < index; i++) {
            result.append(pairs[i][0].toString());
            result.append(" : ");
            result.append(pairs[i][1].toString());
            if (i < index - 1) {
                result.append("\n");
            }
        }
        return result.toString();
    }

    @Override
    public int size() {
        return pairs.length;
    }

    @Override
    public Iterator iterator() {
        return null;
    }
}
