package com.epm.lab.collections.dictionaries;

public class Demo {

    public static void main(String[] args) {
        MyMap<String, String> map = new MyMap<>(5);
        map.put("dog", "animal");
        map.put("grass", "plant");
        map.put("tree", "tall");
        map.put("apple", "fruit");
        map.put("orange", "yellow");
        try {
            map.put("extra", "object");
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println("Too many objects!");
        }
        System.out.println(map);
        System.out.println("size: " + map.size());
        System.out.println(map.get("dog"));


        System.out.println();

        MyTree myTree = new MyTree();
        int key;
        myTree.insert(33, 3.75);
        myTree.insert(55, 5.55);
        myTree.insert(95, 9.99);

        key = 95;
        Node nodeFound = myTree.find(key);
        if (nodeFound != null) {
            System.out.println("Node with key: " + key + " found");
        } else {
            System.out.println("Node with key: " + key + " did not find");
        }
    }

}
